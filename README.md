# ReactMedialInfo
## Installing the packages
```
npm install
```
## Starting the application
```
npm start
```
### Setup for production
```
npm build
yarn global add serve

```
#### Serve the build folder with a static server
```
serve -s build