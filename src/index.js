import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'semantic-ui-css/semantic.min.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import {Route, BrowserRouter as Router} from 'react-router-dom';

// Our Components
import Login from './components/Login';
import Details from './components/Details'

ReactDOM.render(
    <Router>
        <div>
            <Route exact path="/" component={App}/>
            <Route exact path="/login" component={Login}/>
            <Route path="/details/:id" component={Details}/>
        </div>
    </Router>
    , document.getElementById('root')
);
registerServiceWorker();
