import React, { Component } from "react";
import "./Login.css";
import { Image } from 'semantic-ui-react'
import ItemsService from "./ItemsService";
import { DOMAIN_URL } from '../constants';
import AuthService from './AuthService';

const Auth = new AuthService();

class Details extends Component {
    constructor(props) {
        super(props);
        this.ItemService = new ItemsService();
        this.state = {
            isLoading: false,
            itemId: this.props.match.params.id,
            item: this.props.location.state.selectedItem
        };
    }

    componentDidMount() {
        this.setState({ isLoading: true });
        this.getData(this.state.itemId);
    }

    handleLogout() {
        Auth.logout();
        this.props.history.replace('/login');
    }

    render() {
        const { description, isLoading } = this.state;

        if (isLoading) {
            return <div className="page-wrapper item-list">
                <div className="ui container"><p>Loading ...</p></div>
            </div>;
        }

        return (

            <div className="page-wrapper item-list">

                <div className="ui container">
                    <h1 className="ui header">MediaINFO, Frontend Developer Candidate Test (React) <a onClick={this.handleLogout.bind(this)} className="ui tiny teal button logout" role="button">Logout</a></h1>

                    <br />

                    <div className="ui text container">


                        <div className="ui attached segment">
                            <h2 className="ui header">{this.state.item.title}</h2>
                        </div>
                        <div className="ui attached segment">
                            <Image src={DOMAIN_URL + this.state.item.thumbnail} />
                        </div>
                        <div className="ui attached segment" >
                            <span dangerouslySetInnerHTML={{ __html: description != null ? description : '' }}></span>
                        </div>
                        <div className="ui attached segment">
                            <button className="ui button teal" onClick={this.props.history.goBack}>Back</button>
                        </div>

                    </div>

                </div>

            </div>

        );
    }

    getData(itemID) {

        this.ItemService.getItem(itemID)
            .then(res => {
                let description = res.description.values[0];
                this.setState({ description: description, isLoading: false });
            })
            .catch(err => {
                alert(err);
            });
    }
}

export default Details;
