import React, { Component } from "react";
import "./Login.css";
import AuthService from "./AuthService";

class Login extends Component {
    constructor() {
        super();
        this.handleChange = this.handleChange.bind(this);
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
        this.Auth = new AuthService();
    }

    componentWillMount() {
        if (this.Auth.loggedIn()) {
            this.props.history.replace("/");
        } else {
            return false;
        }
    }

    render() {
        return (

            <div className="ui center aligned middle aligned grid login">
                <div className="column loginColumn">
                    <h1>Login</h1>

                    <form className="ui large form" onSubmit={this.handleFormSubmit}>
                        <div className="ui stacked segment">
                            <div className="field">
                                <div className="ui fluid left icon input">
                                    <input type="text"
                                        placeholder="Username goes here..."
                                        name="username"
                                        onChange={this.handleChange}
                                    />
                                    <i aria-hidden="true" className="user icon"></i></div>
                            </div>
                            <div className="field">
                                <div className="ui fluid left icon input">
                                    <input type="password"
                                        placeholder="Password goes here..."
                                        name="password"
                                        onChange={this.handleChange}
                                    />
                                    <i aria-hidden="true" className="lock icon"></i></div>
                            </div>
                            <button className="ui teal large fluid button">Login</button>
                        </div>
                    </form>
                </div>

            </div>
        );
    }

    handleFormSubmit(e) {
        e.preventDefault();

        this.Auth.login(this.state.username, this.state.password)
            .then(res => {
                this.props.history.replace("/");
            })
            .catch(err => {
                alert(err);
            });
    }

    handleChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        });
    }
}

export default Login;
