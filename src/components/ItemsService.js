import cookie from "react-cookies";

export default class ItemsService {
    constructor() {
        this.domain = "https://demo.mediainfo.com/api";
        this.getSession = this.getSession.bind(this);
        this.getItems = this.getItems.bind(this);
    }
    getSession() {
        // Retrieves the user session from cookie
        return cookie.load("MISESSID");
    }
    getItems() {
        const headers = {
            Accept: "application/json",
            "Content-Type": "application/json"
        };
        headers["Authorization"] = "Bearer " + this.getSession();
        return fetch(`${this.domain}/browse`, {
            method: "POST",
            headers: headers,
            body: JSON.stringify({
                limit: 99
            })
        })
            .then(response => response.json())
            .then(data => data)
            .catch(err => {
                console.log('getting error', err)
            });
    }
    getItem(itemId) {
        const headers = {
            Accept: "application/json",
            "Content-Type": "application/json"
        };
        headers["Authorization"] = "Bearer " + this.getSession();
        return fetch(`${this.domain}/item/${itemId}/meta`, {
            headers: headers
        })
            .then(response => response.json())
            .then(data => data)
            .catch(err => {
                console.log('getting error', err)
            });
    }
}