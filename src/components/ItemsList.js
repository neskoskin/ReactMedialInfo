import React, { Component } from 'react';
import { Grid, Segment, Form, Input, Radio } from 'semantic-ui-react'
import ItemsService from './ItemsService';
import { DOMAIN_URL } from '../constants';
import { Link } from 'react-router-dom';

const ItemService = new ItemsService();

class ItemsList extends Component {

    constructor() {
        super();
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleRadioChange = this.handleRadioChange.bind(this);
        this.items = [];
        this.state = {
            items: [],
            selectedItem: {},
            backgroundSize: 'cover'
        }
    }

    componentDidMount() {
        ItemService.getItems()
            .then(response => {

                this.items = response.results.docs;
                this.setState({
                    items: response.results.docs
                })
            })
    }

    handleInputChange(e) {
        if (e.target.value >= 6 && e.target.value <= 99) {
            this.setState({ items: this.items.slice(0, e.target.value) })
        }
        else {
            this.setState({ items: this.items })
        }
    }
    handleRadioChange(e, data) {
        if (data.checked) {
            this.setState({ backgroundSize: 'contain' })
        } else {
            this.setState({ backgroundSize: 'cover' })
        }
    }
    renderItems() {

        return this.state.items.map((item, i) =>
            <div key={i} className="row">
                <div className="column six wide">
                    <div style={{
                        backgroundImage: 'url(' + DOMAIN_URL + item.thumbnail + ')',
                        backgroundSize: this.state.backgroundSize,
                        backgroundPosition: 'center center',
                        backgroundRepeat: 'no-repeat',
                        minWidth: 200,
                        minHeight: 200
                    }}>
                        <Segment style={{ margin: '0 auto', maxWidth: '100px' }}>
                            <p>Index: <strong>{i}</strong></p>
                            <p>Id: <strong>{item.obj_id}</strong></p>
                        </Segment>
                    </div>
                </div>

                <div className="column seven wide">
                    <h3>{item.obj_id}:: {item.title}</h3>
                    <span dangerouslySetInnerHTML={{ __html: item.description }} />
                </div>

                <div className="column three wide ">
                    <div className="ui center aligned">
                        <Link to={{
                            pathname: '/details/' + item.obj_id,
                            state: {
                                selectedItem: item
                            }
                        }} className="ui teal button" role="button">View Details</Link>
                    </div>
                </div>
            </div>
        )
    }

    render() {
        if (this.state.items.length === 0) {
            return null
        }
        return (
            <div className="Items">
                <Form className='topNavigation'>
                    <Form.Group inline>
                        <Form.Field>
                            <label>Number of fields:</label>
                            <Input type="number" onChange={this.handleInputChange} placeholder='Insert number' />
                        </Form.Field>
                        <Form.Field>
                            <label>Bg Cover</label>
                            <Radio label="Bg Contain" toggle onChange={(e, data) => this.handleRadioChange(e, data)} />
                        </Form.Field>
                    </Form.Group>
                </Form>
                <Grid divided stackable columns={3}>
                    {this.renderItems()}
                </Grid>
            </div>
        );
    }
}

export default ItemsList;