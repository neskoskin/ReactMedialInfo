import cookie from "react-cookies";

export default class AuthService {
    constructor(domain) {
        //  this.domain = domain || 'http://localhost:8080'
        this.domain = "https://demo.mediainfo.com/api";
        this.fetch = this.fetch.bind(this);
        this.login = this.login.bind(this);
        this.getProfile = this.getProfile.bind(this);
    }

    login(username, password) {
        // Get a token
        return this.fetch(`${this.domain}/user/login`, {
            method: "POST",
            body: JSON.stringify({
                username,
                password
            })
        }).then(res => {
            this.setSession(res.session);
            return Promise.resolve(res);
        });
    }

    getDetails(ItemID) {
        // Get a token
        return this.fetch(`${this.domain}/item/${ItemID}/meta`).then(res => {
            return Promise.resolve(res);
        });
    }

    loggedIn() {
        // Checks if there is a saved session
        // TODO: Check if it's still valid
        const session = this.getSession();
        if (session) {
            return !!session; // handwaiving here
        }
        return false;
    }

    // isSessionExpired(session) {
    //   try {
    //     var decoded = jwt_decode(session);
    //     if (decoded.exp < Date.now() / 1000) {
    //       console.log("decoded here");
    //       return true;
    //     } else {
    //       console.log("not decoded here");
    //       return false;
    //     }
    //   } catch (err) {
    //     return false;
    //   }
    // }

    setSession(idSession) {
        // Saves user session cookie
        cookie.save("MISESSID", idSession, { path: "/" });
    }

    getSession() {
        // Retrieves the user session from cookie
        return cookie.load("MISESSID");
    }

    logout() {
        // Clear user session cookie
        cookie.remove("MISESSID", { path: "/" });
    }

    getProfile() {
        return this.getSession();
    }

    fetch(url, options) {
        // performs api calls sending the required authentication headers
        const headers = {
            Accept: "application/json",
            "Content-Type": "application/json"
        };

        if (this.loggedIn()) {
            headers["Authorization"] = "Bearer " + this.getSession();
        }

        return fetch(url, {
            headers,
            ...options
        })
            .then(this._checkStatus)
            .then(response => response.json());
    }

    _checkStatus(response) {
        // raises an error in case response status is not a success
        if (response.status === 200) {
            return response;
        } else {
            var error = new Error(response.statusText);
            error.response = response;
            throw error;
        }
    }
}
