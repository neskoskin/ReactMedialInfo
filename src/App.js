import React, { Component } from 'react';
import './App.css';
import AuthService from './components/AuthService';
import withAuth from './components/withAuth';
import ItemsList from './components/ItemsList';

const Auth = new AuthService();

class App extends Component {


    constructor() {
        super();
        this.state = {
            isLoading: false
        };
    }

    componentDidMount() {
        this.setState({ isLoading: true });
    }

    handleLogout() {
        Auth.logout()
        this.props.history.replace('/login');
    }

    render() {
        return (
            <div className="page-wrapper item-list">

                <div className="ui container">
                    <h4 className="ui header"><span className="title-text">MediaINFO Test App</span> <a onClick={this.handleLogout.bind(this)} className="ui tiny teal button logout" role="button">Logout</a></h4>

                    <br />

                    <div className="ui stackable three column grid">
                        <ItemsList user={this.props.user} />
                    </div>

                </div>

            </div>
        );
    }
}

export default withAuth(App);
